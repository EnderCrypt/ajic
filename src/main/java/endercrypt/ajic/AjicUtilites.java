package endercrypt.ajic;


import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.sun.jdi.Method;


public class AjicUtilites
{
	private AjicUtilites()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static String toStringMethods(Collection<Method> methods)
	{
		StringBuilder builder = new StringBuilder();
		List<Method> sortedMethods = methods
			.stream()
			.sorted(AjicUtilites::sortMethodsByName)
			.toList();
		
		for (Method method : sortedMethods)
		{
			builder.append(toStringMethod(method));
			builder.append("\n");
		}
		
		if (builder.length() > 0)
		{
			builder.setLength(builder.length() - 1);
		}
		
		return builder.toString();
	}
	
	private static String toStringMethod(Method method)
	{
		StringBuilder builder = new StringBuilder();
		if (method.isStatic())
		{
			builder.append("static ");
		}
		builder.append(method.name());
		builder.append("(");
		Iterator<String> argumentIterator = method.argumentTypeNames().iterator();
		while (argumentIterator.hasNext())
		{
			String argument = argumentIterator.next();
			builder.append(argument);
			if (argumentIterator.hasNext())
			{
				builder.append(", ");
			}
		}
		builder.append(")");
		if (method.name().equals("<init>") == false)
		{
			builder.append(" -> ");
			builder.append(method.returnTypeName());
		}
		return builder.toString();
	}
	
	private static int sortMethodsByName(Method m1, Method m2)
	{
		String n1 = m1.name();
		String n2 = m2.name();
		
		int sortByConstructor = Boolean.compare(m2.isConstructor(), m1.isConstructor());
		if (sortByConstructor != 0)
			return sortByConstructor;
		
		int sortByStatic = Boolean.compare(m2.isStatic(), m1.isStatic());
		if (sortByStatic != 0)
			return sortByStatic;
		
		return n1.compareTo(n2);
	}
}
