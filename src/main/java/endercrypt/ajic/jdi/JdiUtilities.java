package endercrypt.ajic.jdi;


import endercrypt.ajic.remote.object.RemoteObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.luaj.vm2.LuaBoolean;
import org.luaj.vm2.LuaDouble;
import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaNil;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaValue;

import com.sun.jdi.Type;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.event.Event;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;


public class JdiUtilities
{
	@SuppressWarnings("unchecked")
	public static <T extends Event> T awaitEvent(VirtualMachine virtualMachine, Class<T> targetEventClass) throws InterruptedException
	{
		while (true)
		{
			for (Event event : virtualMachine.eventQueue().remove())
			{
				if (targetEventClass.isInstance(event))
				{
					T targetEvent = (T) event;
					targetEvent.request().disable();
					return targetEvent;
				}
			}
		}
	}
	
	public static Optional<Value> convert(VirtualMachine virtualMachine, Type targetType, LuaValue value)
	{
		if (value instanceof LuaNil nil) // TODO: figure me out
			return Optional.of(virtualMachine.mirrorOfVoid()); // i dont think this is correct!
		
		// System.out.println("check: " + referenceType.name() + " - " + value.getClass().getName());
		JdiConverter jdiConverter = converters.get(new JdiConvertKey(targetType.name(), value.getClass()));
		
		// if nothing was found, lets try forcing into an object
		if (jdiConverter == null && value instanceof RemoteObject remoteObject)
			return Optional.of(remoteObject.getReference());
		
		if (jdiConverter == null)
			return Optional.empty();
		
		return Optional.of(jdiConverter.convert(virtualMachine, value));
	}
	
	private static final Map<JdiConvertKey, JdiConverter> converters = new HashMap<>();
	static
	{
		converters.put(new JdiConvertKey(CharSequence.class, LuaString.class), (vm, v) -> vm.mirrorOf(v.checkjstring()));
		converters.put(new JdiConvertKey(String.class, LuaString.class), (vm, v) -> vm.mirrorOf(v.checkjstring()));
		
		converters.put(new JdiConvertKey(Byte.class, LuaInteger.class), (vm, v) -> vm.mirrorOf((byte) v.checkint()));
		converters.put(new JdiConvertKey(byte.class, LuaInteger.class), (vm, v) -> vm.mirrorOf((byte) v.checkint()));
		
		converters.put(new JdiConvertKey(Short.class, LuaInteger.class), (vm, v) -> vm.mirrorOf((short) v.checkint()));
		converters.put(new JdiConvertKey(short.class, LuaInteger.class), (vm, v) -> vm.mirrorOf((short) v.checkint()));
		
		converters.put(new JdiConvertKey(Integer.class, LuaInteger.class), (vm, v) -> vm.mirrorOf(v.checkint()));
		converters.put(new JdiConvertKey(int.class, LuaInteger.class), (vm, v) -> vm.mirrorOf(v.checkint()));
		
		converters.put(new JdiConvertKey(Long.class, LuaDouble.class), (vm, v) -> vm.mirrorOf(v.checklong()));
		converters.put(new JdiConvertKey(long.class, LuaDouble.class), (vm, v) -> vm.mirrorOf(v.checklong()));
		
		converters.put(new JdiConvertKey(Float.class, LuaDouble.class), (vm, v) -> vm.mirrorOf((float) v.checkdouble()));
		converters.put(new JdiConvertKey(float.class, LuaDouble.class), (vm, v) -> vm.mirrorOf((float) v.checkdouble()));
		
		converters.put(new JdiConvertKey(Double.class, LuaDouble.class), (vm, v) -> vm.mirrorOf(v.checkdouble()));
		converters.put(new JdiConvertKey(double.class, LuaDouble.class), (vm, v) -> vm.mirrorOf(v.checkdouble()));
		
		converters.put(new JdiConvertKey(Boolean.class, LuaBoolean.class), (vm, v) -> vm.mirrorOf(v.checkboolean()));
		converters.put(new JdiConvertKey(boolean.class, LuaBoolean.class), (vm, v) -> vm.mirrorOf(v.checkboolean()));
	}
	
	@RequiredArgsConstructor
	@EqualsAndHashCode
	private static class JdiConvertKey
	{
		private final String targetClass;
		private final Class<? extends LuaValue> sourceClass;
		
		public JdiConvertKey(Class<?> targetClass, Class<? extends LuaValue> sourceClass)
		{
			this(targetClass.getName(), sourceClass);
		}
	}
	
	@FunctionalInterface
	private interface JdiConverter
	{
		public Value convert(VirtualMachine virtualMachine, LuaValue value);
	}
}
