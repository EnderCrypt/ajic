package endercrypt.ajic;


import endercrypt.ajic.arguments.Arguments;
import endercrypt.ajic.arguments.Script;
import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.AjicLibrary;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.launcher.LinearApplication;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Map;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.JsePlatform;

import com.sun.jdi.Bootstrap;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.VirtualMachineManager;
import com.sun.jdi.connect.AttachingConnector;
import com.sun.jdi.connect.Connector.Argument;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;


public class Main extends LinearApplication<Arguments>
{
	public static void main(String[] args)
	{
		LinearApplication.launch(Main.class, args);
	}
	
	@Override
	public void main(Arguments arguments, Information information) throws Exception
	{
		// JDWP
		VirtualMachine virtualMachine = connect(arguments);
		
		// ajic
		AjicContext context = new AjicContext(virtualMachine);
		
		// lua
		Globals globals = JsePlatform.debugGlobals();
		AjicLibrary library = new AjicLibrary(context);
		AjicLibrary.install(globals, library);
		
		// script
		Script script = arguments.getScript();
		LuaFunction luaScript = script.loadAsLuaFunction(globals);
		Varargs luaScriptArguments = arguments.getScriptArgumentsAsVarargs();
		luaScript.invoke(luaScriptArguments);
	}
	
	private static VirtualMachine connect(Arguments arguments) throws IOException, IllegalConnectorArgumentsException
	{
		VirtualMachineManager virtualMachineManager = Bootstrap.virtualMachineManager();
		AttachingConnector socketAttacher = virtualMachineManager.attachingConnectors()
			.stream()
			.filter(connector -> connector.name().equals("com.sun.jdi.SocketAttach"))
			.findFirst().orElseThrow(() -> new AjicException("Failed to find a Socket connector for JDWP"));
		
		Map<String, Argument> socketArguments = socketAttacher.defaultArguments();
		socketArguments.get("hostname").setValue(arguments.getAddress());
		socketArguments.get("port").setValue(String.valueOf(arguments.getPort()));
		
		try
		{
			return socketAttacher.attach(socketArguments);
		}
		catch (ConnectException e)
		{
			String debugArguments = "-Xrunjdwp:transport=dt_socket,address=" + arguments.getAddress() + ":" + arguments.getPort() + ",server=y,suspend=n";
			System.out.println("Launch the target JVM with VM args:");
			System.out.println(debugArguments);
			System.exit(1);
			return null;
		}
	}
}
