package endercrypt.ajic.libraray;


import endercrypt.ajic.remote.RemoteReference;

import java.util.HashMap;
import java.util.Map;

import com.sun.jdi.Mirror;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ReferenceCache<R extends Mirror, L extends RemoteReference<?>>
{
	private final Map<String, L> values = new HashMap<>();
	
	private final AjicContext context;
	private final Factory<R, L> factory;
	
	public L fetch(R reference, String key)
	{
		L value = values.get(key);
		if (value == null)
		{
			value = factory.create(context, reference);
			values.put(key, value);
		}
		return value;
	}
	
	public interface Factory<R extends Mirror, L extends RemoteReference<?>>
	{
		public L create(AjicContext context, R reference);
	}
}
