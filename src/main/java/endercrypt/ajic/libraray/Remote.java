package endercrypt.ajic.libraray;


import org.luaj.vm2.LuaTable;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public abstract class Remote extends LuaTable
{
	@NonNull
	private final AjicContext context;
}
