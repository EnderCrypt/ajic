package endercrypt.ajic.libraray;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.lua.DynamicLibraryLoader;
import endercrypt.ajic.lua.DynamicLibraryLoader.LuaLibrary;
import endercrypt.ajic.lua.LuaExport;
import endercrypt.ajic.remote.clazz.RemoteClass;

import java.util.Collection;
import java.util.function.Function;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaBoolean;
import org.luaj.vm2.LuaDouble;
import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import com.sun.jdi.ModuleReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.ThreadReference;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class AjicLibrary
{
	public static void install(Globals globals, AjicLibrary ajicLibrary)
	{
		LuaLibrary luaLibrary = DynamicLibraryLoader.create("ajic", ajicLibrary);
		globals.load(luaLibrary);
	}
	
	private final AjicContext context;
	
	@LuaExport
	@NoArgsConstructor
	public class Suspend extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			context.getVirtualMachine().suspend();
			return LuaValue.NIL;
		}
	}
	
	@LuaExport
	@NoArgsConstructor
	public class Resume extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			context.getVirtualMachine().resume();
			return LuaValue.NIL;
		}
	}
	
	public static <T> LuaTable toStringTable(Collection<T> objects, Function<T, String> converter)
	{
		return toTable(objects, (input) -> LuaValue.valueOf(converter.apply(input)));
	}
	
	public static <I, O extends LuaValue> LuaTable toTable(Collection<I> objects, Function<I, O> converter)
	{
		LuaValue[] classes = objects.stream()
			.map(converter)
			.sorted(AjicLibrary::compare)
			.toArray(LuaValue[]::new);
		
		return LuaTable.listOf(classes);
	}
	
	private static int compare(Object o1, Object o2)
	{
		String s1 = String.valueOf(o1);
		String s2 = String.valueOf(o2);
		return s1.compareTo(s2);
	}
	
	@LuaExport
	public class FindAllClasses extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			return toStringTable(context.getVirtualMachine().allClasses(), ReferenceType::name);
		}
	}
	
	@LuaExport
	public class FindAllModules extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			return toStringTable(context.getVirtualMachine().allModules(), ModuleReference::name);
		}
	}
	
	@LuaExport
	public class FindAllThreads extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			return toStringTable(context.getVirtualMachine().allThreads(), ThreadReference::name);
		}
	}
	
	@LuaExport
	public class FindClass extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg)
		{
			String className = arg.checkjstring();
			ReferenceType referenceType = context.createReferenceType(className).orElse(null);
			if (referenceType == null)
				return LuaValue.NIL;
			return context.createRemoteType(referenceType);
		}
	}
	
	@LuaExport
	public class Box extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg)
		{
			if (arg instanceof LuaInteger value)
				return perform(value);
			
			if (arg instanceof LuaDouble value)
				return perform(value);
			
			if (arg instanceof LuaBoolean value)
				return perform(value);
			
			throw new AjicException("Failed to box " + arg.getClass());
		}
		
		private LuaValue perform(Object value)
		{
			String boxName = value.getClass().getName();
			RemoteClass remoteClass = (RemoteClass) getContext().createRemoteType(boxName);
			Varargs args = LuaValue.varargsOf(new LuaValue[] {});
			Varargs result = remoteClass.get("new").invoke(args);
			return result.arg(1);
		}
	}
}
