package endercrypt.ajic.libraray;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.jdi.JdiUtilities;
import endercrypt.ajic.lua.DynamicLibraryLoader;
import endercrypt.ajic.remote.object.RemoteObject;
import endercrypt.ajic.remote.type.RemoteType;

import java.util.List;
import java.util.Optional;

import org.luaj.vm2.LuaValue;

import com.sun.jdi.BooleanValue;
import com.sun.jdi.ByteValue;
import com.sun.jdi.DoubleValue;
import com.sun.jdi.FloatValue;
import com.sun.jdi.IntegerValue;
import com.sun.jdi.LongValue;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.ShortValue;
import com.sun.jdi.StringReference;
import com.sun.jdi.Type;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.VoidValue;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class AjicContext
{
	private final VirtualMachine virtualMachine;
	
	public LuaValue convertToLua(Value value)
	{
		if (value == null)
			return LuaValue.NIL;
		
		if (value instanceof StringReference stringReference)
			return LuaValue.valueOf(stringReference.value());
		
		if (value instanceof ObjectReference objectReference)
			return createRemoteObject(objectReference);
		
		if (value instanceof ByteValue byteValue)
			return LuaValue.valueOf(byteValue.value());
		
		if (value instanceof ShortValue shortValue)
			return LuaValue.valueOf(shortValue.value());
		
		if (value instanceof IntegerValue integerValue)
			return LuaValue.valueOf(integerValue.value());
		
		if (value instanceof LongValue longValue)
			return LuaValue.valueOf(longValue.value());
		
		if (value instanceof FloatValue floatValue)
			return LuaValue.valueOf(floatValue.value());
		
		if (value instanceof DoubleValue doubleValue)
			return LuaValue.valueOf(doubleValue.value());
		
		if (value instanceof BooleanValue booleanValue)
			return LuaValue.valueOf(booleanValue.value());
		
		if (value instanceof VoidValue voidValue)
			return LuaValue.NIL;
		
		throw new AjicException("Unknown value type: " + value.getClass().getName());
	}
	
	public Optional<Value> convertToJdi(LuaValue value, Type targetType)
	{
		return JdiUtilities.convert(virtualMachine, targetType, value);
	}
	
	public Optional<ReferenceType> createReferenceType(String typeName)
	{
		List<ReferenceType> referenceTypes = virtualMachine.classesByName(typeName);
		if (referenceTypes.isEmpty())
			return Optional.empty();
		
		if (referenceTypes.size() > 1)
			throw new AjicException("Did not expect to find more than 1 reference type: " + referenceTypes);
		
		ReferenceType referenceType = referenceTypes.stream().findFirst().orElseThrow();
		return Optional.of(referenceType);
	}
	
	public RemoteObject createRemoteObject(ObjectReference reference)
	{
		RemoteObject remote = new RemoteObject(this, reference);
		DynamicLibraryLoader.inject(remote, remote);
		return remote;
	}
	
	private final ReferenceCache<ReferenceType, RemoteType<?>> typeCache = new ReferenceCache<>(this, RemoteType::create);
	
	public RemoteType<?> createRemoteType(String typeName)
	{
		ReferenceType referenceType = createReferenceType(typeName).orElseThrow(() -> new AjicException("Unknown type: " + typeName));
		return createRemoteType(referenceType);
	}
	
	public RemoteType<?> createRemoteType(ReferenceType reference)
	{
		String key = reference.name();
		RemoteType<?> remote = typeCache.fetch(reference, key);
		DynamicLibraryLoader.inject(remote, remote);
		return remote;
	}
}
