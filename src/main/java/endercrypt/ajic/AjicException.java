package endercrypt.ajic;

public class AjicException extends RuntimeException
{
	private static final long serialVersionUID = 7749752228261635781L;
	
	public AjicException(String message)
	{
		super(message);
	}
	
	public AjicException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
