package endercrypt.ajic.arguments;


import endercrypt.library.groundwork.command.StandardArguments;

import java.util.List;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;

import lombok.Getter;
import picocli.CommandLine.Help;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;


@Getter
public class Arguments extends StandardArguments
{
	@Option(names = { "-a", "--address" }, description = "Address of the JDWP process", showDefaultValue = Help.Visibility.ALWAYS)
	private String address = "127.0.0.1";
	
	@Option(names = { "-p", "--port" }, description = "Port of the JDWP process", showDefaultValue = Help.Visibility.ALWAYS)
	private int port = 55123;
	
	@Parameters(arity = "1", converter = Script.Converter.class)
	private Script script;
	
	@Parameters(arity = "0..*")
	private List<String> scriptArguments;
	
	public Varargs getScriptArgumentsAsVarargs()
	{
		LuaValue[] varargsArguments = getScriptArguments()
			.stream()
			.skip(1) // the script name itself
			.map(LuaValue::valueOf)
			.toArray(LuaValue[]::new);
		
		return LuaValue.varargsOf(varargsArguments);
	}
}
