package endercrypt.ajic.arguments;


import endercrypt.library.commons.JarInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import picocli.CommandLine;
import picocli.CommandLine.ITypeConverter;


@Getter
@RequiredArgsConstructor
public abstract class Script
{
	private final String name;
	private final String contents;
	
	public Script(byte[] contents)
	{
		this("stdin", new String(contents));
	}
	
	public LuaFunction loadAsLuaFunction(Globals globals)
	{
		return (LuaFunction) globals.load(getContents(), getName(), globals);
	}
	
	@Getter
	public static class ScriptFromFile extends Script
	{
		public ScriptFromFile(Path path) throws IOException
		{
			super(path.toString(), Files.readString(path));
		}
	}
	
	@Getter
	public static class ScriptFromStdin extends Script
	{
		public ScriptFromStdin() throws IOException
		{
			super(System.in.readAllBytes());
		}
	}
	
	public static class Converter implements ITypeConverter<Script>
	{
		@Override
		public Script convert(String value) throws Exception
		{
			try
			{
				return read(value);
			}
			catch (IOException e)
			{
				String cause = e.getClass().getSimpleName() + ": " + e.getMessage();
				throw new CommandLine.TypeConversionException("Failed to read script due to " + cause);
			}
		}
		
		private Script read(String value) throws IOException
		{
			if (value.equals("-"))
				return new ScriptFromStdin();
			
			Path fullPath = JarInfo.workingDirectory.resolve(value).normalize();
			if (Files.isRegularFile(fullPath))
				return new ScriptFromFile(fullPath);
			
			throw new CommandLine.TypeConversionException("Could not locate or read script at: " + fullPath);
		}
	}
}
