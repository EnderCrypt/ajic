package endercrypt.ajic.lua;


import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;


public class LuaUtilities
{
	@SuppressWarnings("unchecked")
	public static <T extends LuaValue> T check(Class<T> luaClass, LuaValue value)
	{
		if (luaClass.isInstance(value) == false)
			throw new LuaError("Expected argument of type " + luaClass.getSimpleName());
		
		return (T) value;
	}
}
