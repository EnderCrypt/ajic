package endercrypt.ajic.lua;


import endercrypt.ajic.AjicException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Consumer;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.LibFunction;
import org.luaj.vm2.lib.TwoArgFunction;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


public class DynamicLibraryLoader
{
	public static void inject(LuaTable table, Object source)
	{
		Class<?> sourceClass = source.getClass();
		
		Consumer<LibraryEntry> addToTable = (entry) -> table.set(entry.getName(), entry.getValue());
		
		for (Class<?> innerClass : sourceClass.getClasses())
		{
			parseClass(innerClass, source).ifPresent(addToTable);
		}
		for (Field innerField : sourceClass.getFields())
		{
			parseField(innerField, source).ifPresent(addToTable);
		}
		
	}
	
	public static LuaLibrary create(String name, Object object)
	{
		LuaTable table = new LuaTable();
		inject(table, object);
		return new LuaLibrary(name, table);
	}
	
	@Getter
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public static class LuaLibrary extends TwoArgFunction
	{
		private final String name;
		private final LuaTable table;
		
		@Override
		public LuaValue call(LuaValue modname, LuaValue env)
		{
			Globals globals = env.checkglobals();
			globals.set(name, table);
			return table;
		}
	}
	
	private static String cleanName(String name)
	{
		char firstLetter = name.charAt(0);
		String theRest = name.substring(1);
		name = Character.toLowerCase(firstLetter) + theRest;
		return name;
	}
	
	private static Optional<LibraryEntry> parseClass(Class<?> innerClass, Object object)
	{
		if (innerClass.isAnnotationPresent(LuaExport.class) == false)
			return Optional.empty();
		
		if (LibFunction.class.isAssignableFrom(innerClass) == false)
			return Optional.empty();
		
		String name = cleanName(innerClass.getSimpleName());
		Constructor<?> constructor = innerClass.getConstructors()[0];
		try
		{
			LuaFunction function = (LuaFunction) constructor.newInstance(object);
			return Optional.of(new LibraryEntry(name, function));
		}
		catch (ReflectiveOperationException e)
		{
			throw new AjicException("Failed to parse java class: " + innerClass, e);
		}
	}
	
	private static Optional<LibraryEntry> parseField(Field innerField, Object object)
	{
		if (innerField.isAnnotationPresent(LuaExport.class) == false)
			return Optional.empty();
		
		if (innerField.getDeclaringClass().isAssignableFrom(LuaValue.class) == false)
			return Optional.empty();
		
		String name = innerField.getName();
		try
		{
			innerField.setAccessible(true);
			LuaValue value = (LuaValue) innerField.get(object);
			return Optional.of(new LibraryEntry(name, value));
		}
		catch (ReflectiveOperationException e)
		{
			throw new AjicException("Failed to parse java field: " + innerField, e);
		}
	}
	
	@Getter
	@RequiredArgsConstructor
	private static class LibraryEntry
	{
		private final String name;
		private final LuaValue value;
	}
}
