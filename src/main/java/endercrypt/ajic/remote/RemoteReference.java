package endercrypt.ajic.remote;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.Remote;

import com.sun.jdi.Mirror;

import lombok.Getter;
import lombok.NonNull;


@Getter
public abstract class RemoteReference<T extends Mirror> extends Remote
{
	@NonNull
	private final T reference;
	
	public RemoteReference(AjicContext context, T reference)
	{
		super(context);
		this.reference = reference;
	}
}
