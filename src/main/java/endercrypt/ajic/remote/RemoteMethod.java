package endercrypt.ajic.remote;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.jdi.JdiUtilities;
import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.Remote;

import java.util.ArrayList;
import java.util.List;

import org.luaj.vm2.LuaValue;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.InternalException;
import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.InvocationException;
import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Type;
import com.sun.jdi.Value;
import com.sun.jdi.event.MethodEntryEvent;

import lombok.Getter;
import lombok.SneakyThrows;


@Getter
public abstract class RemoteMethod extends Remote
{
	private final String name;
	protected final Method method;
	
	public RemoteMethod(AjicContext context, Method method)
	{
		super(context);
		this.method = method;
		
		this.name = method.name();
		set("name", name);
		
		set("returnType", method.returnTypeName());
		
		set("static", LuaValue.valueOf(method.isStatic()));
	}
	
	public Value luaInvokeRemote(List<? extends LuaValue> luaArguments) throws InvalidTypeException
	{
		try
		{
			List<Value> valueArguments = new ArrayList<>();
			List<Type> expectedArguments = method.argumentTypes();
			
			if (expectedArguments.size() != luaArguments.size())
			{
				throw new InvalidTypeException(); // TODO: support VARARGS
			}
			for (int i = 0; i < luaArguments.size(); i++)
			{
				LuaValue luaValue = luaArguments.get(i);
				Type targetType = expectedArguments.get(i);
				Value value = getContext().convertToJdi(luaValue, targetType).orElseThrow(() -> new InvalidTypeException());
				valueArguments.add(value);
			}
			return invokeRemote(valueArguments);
		}
		catch (ClassNotLoadedException e)
		{
			throw new InvalidTypeException("The parameters for the method had non-existant types so this could not be what was intended to be called");
		}
	}
	
	public Value invokeRemote(List<? extends Value> valueArguments) throws InvalidTypeException
	{
		ThreadReference threadReference = stealRandomThreadInDebugMode();
		try
		{
			return call(threadReference, method, valueArguments, 0);
		}
		catch (InternalException e)
		{
			if (e.errorCode() == 502)
			{
				throw new AjicException("The previous method invocation did not yet finish or is stalled");
			}
			throw e;
		}
		catch (ClassNotLoadedException | IncompatibleThreadStateException | InvocationException e)
		{
			throw new AjicException("Failed to call " + name + "(...)", e);
		}
		finally
		{
			threadReference.resume();
		}
	}
	
	protected abstract Value call(ThreadReference threadReference, Method method, List<? extends Value> arguments, int options) throws InvalidTypeException, ClassNotLoadedException, IncompatibleThreadStateException, InvocationException;
	
	@SneakyThrows(InterruptedException.class)
	private ThreadReference stealRandomThreadInDebugMode()
	{
		getContext().getVirtualMachine().eventRequestManager().createMethodEntryRequest().enable();
		
		return JdiUtilities.awaitEvent(getContext().getVirtualMachine(), MethodEntryEvent.class).thread();
	}
}
