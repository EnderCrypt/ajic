package endercrypt.ajic.remote;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.AjicUtilites;
import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.Remote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.Method;
import com.sun.jdi.Value;

import lombok.RequiredArgsConstructor;


public abstract class RemoteMethodTable extends Remote
{
	protected final Map<Method, RemoteMethod> methods = new HashMap<>();
	
	public RemoteMethodTable(AjicContext context)
	{
		super(context);
	}
	
	protected void installMethod(Method method)
	{
		String methodName = method.name();
		JavaFunction javaFunction = new JavaFunction(methodName);
		set(methodName, javaFunction); // expected to overwrite any java overloads
		
		RemoteMethod remoteMethod = createRemoteMethod(method);
		methods.put(method, remoteMethod);
	}
	
	protected abstract RemoteMethod createRemoteMethod(Method method);
	
	@Override
	public LuaValue tostring()
	{
		String info = AjicUtilites.toStringMethods(methods.keySet());
		return LuaValue.valueOf(info);
	}
	
	@RequiredArgsConstructor
	protected class JavaFunction extends VarArgFunction
	{
		private final String methodName;
		
		@Override
		public Varargs invoke(Varargs args)
		{
			List<LuaValue> arguments = IntStream.range(1, args.narg() + 1)
				.mapToObj(args::checkvalue)
				.toList();
			
			return invoke(arguments);
		}
		
		private Varargs invoke(List<LuaValue> arguments)
		{
			for (RemoteMethod remoteMethod : methods.values())
			{
				if (remoteMethod.getMethod().name().equals(methodName))
				{
					try
					{
						Value returnValue = remoteMethod.luaInvokeRemote(arguments);
						return getContext().convertToLua(returnValue);
					}
					catch (InvalidTypeException e)
					{
						// Method parameters did not accept our lua parameters
						// we ignore and keep trying other methods
						// (java method overloading)
					}
				}
			}
			throw new AjicException("No such method: " + methodName);
		}
	}
}
