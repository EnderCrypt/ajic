package endercrypt.ajic.remote.object;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.lua.DynamicLibraryLoader;
import endercrypt.ajic.lua.LuaExport;
import endercrypt.ajic.remote.RemoteReference;
import endercrypt.ajic.remote.type.RemoteType;

import org.luaj.vm2.LuaNil;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import com.sun.jdi.ObjectReference;

import lombok.Getter;
import lombok.NonNull;


@Getter
public class RemoteObject extends RemoteReference<ObjectReference>
{
	@NonNull
	private final RemoteType<?> remoteType;
	
	public RemoteObject(AjicContext context, ObjectReference reference)
	{
		super(context, reference);
		
		set("id", LuaValue.valueOf(String.valueOf(reference.uniqueID())));
		
		this.remoteType = context.createRemoteType(reference.referenceType());
		set("class", remoteType);
		
		set("field", new RemoteObjectFieldTable(this));
		
		set("method", new RemoteObjectMethodTable(this));
		
		DynamicLibraryLoader.inject(this, this);
	}
	
	@LuaExport
	public class FindReferringObjects extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg1)
		{
			long limit = getCountFromArg(arg1);
			RemoteObject[] objects = getReference().referringObjects(limit)
				.stream()
				.map(object -> new RemoteObject(getContext(), object))
				.toArray(RemoteObject[]::new);
			
			return LuaValue.listOf(objects);
		}
		
		private long getCountFromArg(LuaValue arg1)
		{
			if (arg1 instanceof LuaNil)
				return Long.MAX_VALUE;
			
			return arg1.checklong();
		}
	}
	
	@LuaExport
	public class Info extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			String info = remoteType.getReference().name() + ": " + getReference().toString();
			return LuaValue.valueOf(info);
		}
	}
}
