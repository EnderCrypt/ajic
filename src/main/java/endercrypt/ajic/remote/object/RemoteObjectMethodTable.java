package endercrypt.ajic.remote.object;


import endercrypt.ajic.remote.RemoteMethod;
import endercrypt.ajic.remote.RemoteMethodTable;

import com.sun.jdi.Method;


public class RemoteObjectMethodTable extends RemoteMethodTable
{
	private final RemoteObject remoteObject;
	
	public RemoteObjectMethodTable(RemoteObject remoteObject)
	{
		super(remoteObject.getContext());
		this.remoteObject = remoteObject;
		
		for (Method method : remoteObject.getRemoteType().getReference().methods())
		{
			if (method.isStatic() == false)
			{
				installMethod(method);
			}
		}
	}
	
	@Override
	protected RemoteMethod createRemoteMethod(Method method)
	{
		return new RemoteObjectMethod(getContext(), remoteObject, method);
	}
}
