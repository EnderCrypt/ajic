package endercrypt.ajic.remote.object;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.RemoteMethod;

import java.util.List;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.InvocationException;
import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;

import lombok.Getter;


@Getter
public class RemoteObjectMethod extends RemoteMethod
{
	private final RemoteObject remoteObject;
	
	public RemoteObjectMethod(AjicContext context, RemoteObject remoteObject, Method method)
	{
		super(context, method);
		this.remoteObject = remoteObject;
	}
	
	@Override
	protected Value call(ThreadReference threadReference, Method method, List<? extends Value> arguments, int options) throws InvalidTypeException, ClassNotLoadedException, IncompatibleThreadStateException, InvocationException
	{
		return remoteObject.getReference().invokeMethod(threadReference, method, arguments, options);
	}
}
