package endercrypt.ajic.remote.object;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.lua.DynamicLibraryLoader;
import endercrypt.ajic.lua.LuaExport;
import endercrypt.ajic.remote.RemoteField;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.Field;
import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.Type;
import com.sun.jdi.Value;

import lombok.Getter;


@Getter
public class RemoteObjectField extends RemoteField
{
	private final RemoteObject remoteObject;
	
	public RemoteObjectField(RemoteObject remoteObject, Field field)
	{
		super(remoteObject.getContext(), field);
		this.remoteObject = remoteObject;
		
		DynamicLibraryLoader.inject(this, this);
	}
	
	@LuaExport
	public class Get extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			Value value = remoteObject.getReference().getValue(field);
			return getContext().convertToLua(value);
		}
	}
	
	@LuaExport
	public class Set extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg1)
		{
			try
			{
				Type targetType = field.type();
				Value value = getContext().convertToJdi(arg1, targetType).orElse(null);
				if (value == null)
					return LuaValue.NIL;
				remoteObject.getReference().setValue(field, value);
			}
			catch (InvalidTypeException | ClassNotLoadedException e)
			{
				throw new AjicException("Attempted to set value on a non-loaded class", e);
			}
			return LuaValue.NIL;
		}
	}
}
