package endercrypt.ajic.remote.object;


import endercrypt.ajic.remote.RemoteField;
import endercrypt.ajic.remote.type.RemoteTypeFieldTable;

import com.sun.jdi.Field;


public class RemoteObjectFieldTable extends RemoteTypeFieldTable
{
	private final RemoteObject remoteObject;
	
	public RemoteObjectFieldTable(RemoteObject remoteObject)
	{
		super(remoteObject.getContext());
		this.remoteObject = remoteObject;
		
		for (Field field : remoteObject.getRemoteType().getReference().allFields())
		{
			if (field.isStatic() == false)
			{
				installField(field);
			}
		}
	}
	
	@Override
	protected RemoteField createRemoteField(Field field)
	{
		return new RemoteObjectField(remoteObject, field);
	}
	
}
