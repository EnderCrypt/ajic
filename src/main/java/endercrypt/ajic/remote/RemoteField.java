package endercrypt.ajic.remote;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.Remote;

import org.luaj.vm2.LuaValue;

import com.sun.jdi.Field;

import lombok.Getter;


@Getter
public abstract class RemoteField extends Remote
{
	protected final Field field;
	
	protected RemoteField(AjicContext context, Field field)
	{
		super(context);
		this.field = field;
		
		set("name", field.name());
		set("returnClass", field.declaringType().name());
		set("static", LuaValue.valueOf(field.isStatic()));
	}
}
