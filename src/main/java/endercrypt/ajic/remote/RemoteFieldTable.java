package endercrypt.ajic.remote;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.libraray.Remote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.luaj.vm2.LuaValue;

import com.sun.jdi.Field;


public abstract class RemoteFieldTable extends Remote
{
	private static final Pattern patternGenericArgument = Pattern.compile("L([a-zA-Z/]+);");
	
	private final Map<Field, RemoteField> fields = new HashMap<>();
	
	public RemoteFieldTable(AjicContext context)
	{
		super(context);
	}
	
	protected void installField(Field field)
	{
		RemoteField remoteField = createRemoteField(field);
		String name = field.name();
		set(name, remoteField);
		fields.put(field, remoteField);
	}
	
	protected abstract RemoteField createRemoteField(Field field);
	
	@Override
	public LuaValue tostring()
	{
		StringBuilder builder = new StringBuilder();
		for (Field field : fields.keySet())
		{
			builder.append(field.typeName());
			String generic = field.genericSignature();
			if (generic != null)
			{
				builder.append("<");
				builder.append(String.join(", ", parseGenericClasses(generic)));
				builder.append(">");
			}
			
			builder.append(" ");
			builder.append(field.name());
			builder.append("\n");
		}
		
		if (builder.length() > 0)
		{
			builder.setLength(builder.length() - 1);
		}
		return LuaValue.valueOf(builder.toString());
	}
	
	private List<String> parseGenericClasses(String genericString)
	{
		int genericIndexStart = genericString.indexOf("<");
		int genericIndexEnd = genericString.indexOf(">", genericIndexStart + 1);
		String genericArgumentString = genericString.substring(genericIndexStart + 1, genericIndexEnd);
		Matcher matcher = patternGenericArgument.matcher(genericArgumentString);
		List<String> argumentClasses = new ArrayList<>();
		while (matcher.find())
		{
			String argumentClass = matcher.group(1);
			argumentClass = argumentClass.replace("/", ".");
			argumentClasses.add(argumentClass);
		}
		return argumentClasses;
	}
}
