package endercrypt.ajic.remote.array;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.type.RemoteType;

import com.sun.jdi.ArrayType;

import lombok.Getter;


@Getter
public class RemoteArray extends RemoteType<ArrayType>
{
	public RemoteArray(AjicContext context, ArrayType reference)
	{
		super(context, reference);
	}
}
