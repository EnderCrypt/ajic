package endercrypt.ajic.remote.type;


import endercrypt.ajic.remote.RemoteField;

import com.sun.jdi.Field;

import lombok.Getter;


@Getter
public abstract class RemoteTypeField extends RemoteField
{
	public RemoteTypeField(RemoteType<?> remoteType, Field field)
	{
		super(remoteType.getContext(), field);
	}
	
}
