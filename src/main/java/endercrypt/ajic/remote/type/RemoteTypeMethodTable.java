package endercrypt.ajic.remote.type;


import endercrypt.ajic.remote.RemoteMethodTable;


public abstract class RemoteTypeMethodTable extends RemoteMethodTable
{
	public RemoteTypeMethodTable(RemoteType<?> remoteType)
	{
		super(remoteType.getContext());
	}
}
