package endercrypt.ajic.remote.type;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.lua.LuaExport;
import endercrypt.ajic.remote.RemoteReference;
import endercrypt.ajic.remote.array.RemoteArray;
import endercrypt.ajic.remote.clazz.RemoteClass;
import endercrypt.ajic.remote.object.RemoteObject;

import org.luaj.vm2.LuaNil;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

import com.sun.jdi.ArrayType;
import com.sun.jdi.ClassType;
import com.sun.jdi.ReferenceType;

import lombok.Getter;


@Getter
public abstract class RemoteType<T extends ReferenceType> extends RemoteReference<T>
{
	public static RemoteType<?> create(AjicContext context, ReferenceType reference)
	{
		if (reference instanceof ClassType classType)
			return new RemoteClass(context, classType);
		
		if (reference instanceof ArrayType arrayType)
			return new RemoteArray(context, arrayType);
		
		throw new AjicException("Unknown type of java-type: " + reference.getClass());
	}
	
	private final String name;
	
	protected RemoteType(AjicContext context, T reference)
	{
		super(context, reference);
		
		this.name = reference.name();
		set("name", name);
	}
	
	@LuaExport
	public class FindObjects extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg1)
		{
			long limit = getCountFromArg(arg1);
			RemoteObject[] objects = getReference().instances(limit)
				.stream()
				.map(object -> new RemoteObject(getContext(), object))
				.toArray(RemoteObject[]::new);
			
			return LuaValue.listOf(objects);
		}
		
		private long getCountFromArg(LuaValue arg1)
		{
			if (arg1 instanceof LuaNil)
				return Long.MAX_VALUE;
			
			return arg1.checklong();
		}
	}
}
