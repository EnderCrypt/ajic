package endercrypt.ajic.remote.type;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.RemoteMethod;

import com.sun.jdi.Method;

import lombok.Getter;


@Getter
public abstract class RemoteTypeMethod extends RemoteMethod
{
	private final RemoteType<?> remoteType;
	
	public RemoteTypeMethod(AjicContext context, RemoteType<?> remoteType, Method method)
	{
		super(context, method);
		this.remoteType = remoteType;
	}
}
