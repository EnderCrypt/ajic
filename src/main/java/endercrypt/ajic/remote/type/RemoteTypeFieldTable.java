package endercrypt.ajic.remote.type;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.RemoteFieldTable;


public abstract class RemoteTypeFieldTable extends RemoteFieldTable
{
	public RemoteTypeFieldTable(AjicContext context)
	{
		super(context);
	}
}
