package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.type.RemoteTypeMethod;

import java.util.List;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.IncompatibleThreadStateException;
import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.InvocationException;
import com.sun.jdi.Method;
import com.sun.jdi.ThreadReference;
import com.sun.jdi.Value;

import lombok.Getter;


@Getter
public class RemoteClassConstructor extends RemoteTypeMethod
{
	private final RemoteClass remoteClass;
	
	public RemoteClassConstructor(AjicContext context, RemoteClass remoteClass, Method method)
	{
		super(context, remoteClass, method);
		this.remoteClass = remoteClass;
	}
	
	@Override
	protected Value call(ThreadReference threadReference, Method method, List<? extends Value> arguments, int options) throws InvalidTypeException, ClassNotLoadedException, IncompatibleThreadStateException, InvocationException
	{
		return remoteClass.getReference().newInstance(threadReference, method, arguments, options);
	}
}
