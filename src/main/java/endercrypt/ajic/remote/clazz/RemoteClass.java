package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.type.RemoteType;

import com.sun.jdi.ClassType;

import lombok.Getter;


@Getter
public class RemoteClass extends RemoteType<ClassType>
{
	public RemoteClass(AjicContext context, ClassType reference)
	{
		super(context, reference);
		
		set("field", new RemoteClassFieldTable(this));
		
		set("method", new RemoteClassMethodTable(this));
		
		set("new", new RemoteClassConstructorTable(this));
	}
}
