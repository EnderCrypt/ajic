package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.lua.DynamicLibraryLoader;
import endercrypt.ajic.lua.LuaExport;
import endercrypt.ajic.remote.type.RemoteTypeField;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import com.sun.jdi.ClassNotLoadedException;
import com.sun.jdi.Field;
import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.Type;
import com.sun.jdi.Value;

import lombok.Getter;


@Getter
public class RemoteClassField extends RemoteTypeField
{
	private final RemoteClass remoteClassType;
	
	public RemoteClassField(RemoteClass remoteClassType, Field field)
	{
		super(remoteClassType, field);
		this.remoteClassType = remoteClassType;
		
		DynamicLibraryLoader.inject(this, this);
	}
	
	@LuaExport
	public class Get extends ZeroArgFunction
	{
		@Override
		public LuaValue call()
		{
			Value value = remoteClassType.getReference().getValue(field);
			return getContext().convertToLua(value);
		}
	}
	
	@LuaExport
	public class Set extends OneArgFunction
	{
		@Override
		public LuaValue call(LuaValue arg1)
		{
			try
			{
				Type targetType = field.type();
				Value value = getContext().convertToJdi(arg1, targetType).orElse(null);
				if (value == null)
					return LuaValue.NIL;
				remoteClassType.getReference().setValue(field, value);
			}
			catch (InvalidTypeException | ClassNotLoadedException e)
			{
				throw new AjicException("Attempted to set value on a non-loaded class", e);
			}
			return LuaValue.NIL;
		}
	}
}
