package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.remote.RemoteMethod;
import endercrypt.ajic.remote.type.RemoteTypeMethodTable;

import com.sun.jdi.Method;


public class RemoteClassMethodTable extends RemoteTypeMethodTable
{
	private final RemoteClass remoteClassType;
	
	public RemoteClassMethodTable(RemoteClass remoteClassType)
	{
		super(remoteClassType);
		this.remoteClassType = remoteClassType;
		
		for (Method method : remoteClassType.getReference().methods())
		{
			if (method.isStatic() == true)
			{
				installMethod(method);
			}
		}
	}
	
	@Override
	protected RemoteMethod createRemoteMethod(Method method)
	{
		return new RemoteClassMethod(getContext(), remoteClassType, method);
	}
}
