package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.remote.RemoteField;
import endercrypt.ajic.remote.RemoteFieldTable;

import com.sun.jdi.Field;


public class RemoteClassFieldTable extends RemoteFieldTable
{
	private final RemoteClass remoteClass;
	
	public RemoteClassFieldTable(RemoteClass remoteClass)
	{
		super(remoteClass.getContext());
		this.remoteClass = remoteClass;
		
		for (Field field : remoteClass.getReference().allFields())
		{
			if (field.isStatic() == true)
			{
				installField(field);
			}
		}
	}
	
	@Override
	protected RemoteField createRemoteField(Field field)
	{
		return new RemoteClassField(remoteClass, field);
	}
	
}
