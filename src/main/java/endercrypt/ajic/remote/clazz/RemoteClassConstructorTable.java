package endercrypt.ajic.remote.clazz;


import endercrypt.ajic.AjicException;
import endercrypt.ajic.AjicUtilites;
import endercrypt.ajic.libraray.AjicContext;
import endercrypt.ajic.remote.RemoteMethod;

import java.util.List;
import java.util.stream.IntStream;

import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

import com.sun.jdi.InvalidTypeException;
import com.sun.jdi.Method;
import com.sun.jdi.Value;

import lombok.Getter;


@Getter
public class RemoteClassConstructorTable extends VarArgFunction
{
	private final AjicContext context;
	private final RemoteClass remoteClass;
	private final List<RemoteClassConstructor> constructors;
	
	public RemoteClassConstructorTable(RemoteClass remoteClass)
	{
		this.context = remoteClass.getContext();
		this.remoteClass = remoteClass;
		this.constructors = remoteClass.getReference()
			.allMethods()
			.stream()
			.filter(Method::isConstructor)
			.map(method -> new RemoteClassConstructor(context, remoteClass, method))
			.toList();
	}
	
	@Override
	public Varargs invoke(Varargs args)
	{
		List<LuaValue> arguments = IntStream.range(1, args.narg() + 1)
			.mapToObj(args::checkvalue)
			.toList();
		
		return invoke(arguments);
	}
	
	private Varargs invoke(List<LuaValue> arguments)
	{
		for (RemoteClassConstructor classConstructor : constructors)
		{
			try
			{
				Value returnValue = classConstructor.luaInvokeRemote(arguments);
				return getContext().convertToLua(returnValue);
			}
			catch (InvalidTypeException e)
			{
				// Method parameters did not accept our lua parameters
				// we ignore and keep trying other methods
				// (java method overloading)
			}
		}
		throw new AjicException("No such constructor");
	}
	
	@Override
	public LuaValue tostring()
	{
		List<Method> methods = constructors.stream()
			.map(RemoteMethod::getMethod)
			.toList();
		String info = AjicUtilites.toStringMethods(methods);
		return LuaValue.valueOf(info);
	}
}
